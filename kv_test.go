package kv

import (
	"errors"
	"fmt"
	"github.com/stretchr/testify/require"
	"strconv"
	"testing"
	"time"
)

/*	fakeEngine emulate ETCD structure
	/
	/data            - dir
	/data/file       - file
	/data/dir        - file
	/fail            - unknown generate error when IsDir
	/list            - dir, generate error when List
	/error_dir       - dir
	/error_dir/dir   - dir, generate error when DeleteDir
	/error_dir1      - dir
	/error_dir1/dir  - dir, generate error when List
	/error_dir2      - dir
	/error_dir2/file - file, generate error when Get
	/error_file      - file
	/error_file/file - file, generate error when Delete
	/recursive       - dir
	/recursive/_data - env file
	/recursive/dir   - dir
	/recursive/dir/q - file
	/recursive/dir/z - file
*/

type fakeEngine struct {
	data *bool
	Engine
}

func (fake *fakeEngine) Init(_ map[string]interface{}) error {
	return nil
}

func (fake *fakeEngine) SetDefaultDir(_ string) error {
	return nil
}

func (fake *fakeEngine) IsDir(path string) (bool, error) {
	if path == "/data" || path == "/list" || path == "/recursive" {
		return true, nil
	} else if path == "/data/file" {
		return false, nil
	} else if path == "/fail" {
		return false, errors.New("")
	}
	return true, nil
}

func (fake *fakeEngine) List(path string) (map[string]bool, error) {
	if path == "/data" {
		result := make(map[string]bool)
		result = map[string]bool{"/data/file": false, "/data/dir": true}
		return result, nil
	} else if path == "/error_dir" {
		result := make(map[string]bool)
		result = map[string]bool{"/error_dir/dir": true}
		return result, nil
	} else if path == "/error_dir1" {
		result := make(map[string]bool)
		result = map[string]bool{"/error_dir1/dir": true}
		return result, nil
	} else if path == "/error_dir1/dir" {
		return nil, errors.New("")
	} else if path == "/error_dir2" {
		result := make(map[string]bool)
		result = map[string]bool{"/error_dir2/file": false}
		return result, nil
	} else if path == "/error_file" {
		result := make(map[string]bool)
		result = map[string]bool{"/error_file/file": false}
		return result, nil
	} else if path == "/recursive" {
		result := make(map[string]bool)
		result = map[string]bool{"/recursive/_data": false, "/recursive/dir": true}
		return result, nil
	} else if path == "/recursive/dir" {
		result := make(map[string]bool)
		result = map[string]bool{"/recursive/dir/q": false, "/recursive/dir/z": false}
		return result, nil
	} else if path == "/list" {
		return map[string]bool{}, errors.New("")
	}
	return map[string]bool{}, nil
}

func (fake *fakeEngine) ReadDir(_ string) (map[string]string, error) {
	return map[string]string{}, nil
}

func (fake *fakeEngine) Get(path string) (string, error) {
	if path == "/recursive/_data" {
		return "{\"datafile\": \"zopa\"}", nil
	} else if path == "/recursive/dir/q" {
		return "{\"username\": \"user\"}", nil
	} else if path == "/recursive/dir/z" {
		return "uahaha", nil
	} else if path == "/error_dir2/file" {
		return "", fmt.Errorf("")
	}
	return "", nil
}

func (fake *fakeEngine) Set(_, _ string) error {
	return nil
}

func (fake *fakeEngine) Delete(path string) error {
	if path == "/error_file/file" {
		return errors.New("")
	}
	return nil
}

func (fake *fakeEngine) CreateDir(_ string) error {
	return nil
}

func (fake *fakeEngine) DeleteDir(path string) error {
	if path == "/error_dir/dir" {
		return errors.New("")
	}
	return nil
}

func (fake *fakeEngine) DeleteDirRecursive(_ string) error {
	return nil
}

func engineInit() {
	var mok Engine
	if kv.engine == nil || engine.data == nil {
		engine = fakeEngine{}
		boo := true
		engine.data = &boo
		mok = &engine
		kv = Storage{}
		_ = kv.SetEngine(mok, nil)
	}
}

var engine fakeEngine
var kv Storage

func TestStorage_SetEngineByName(t *testing.T) {
	var err error
	var settings = map[string]interface{}{}
	settings["address"] = []string{"http://localhost:12345"}
	settings["database"] = "test/database.kdbx"
	settings["password"] = "test"
	kv := Storage{}
	err = kv.SetEngineByName("test", settings)
	require.EqualError(t, err, "unknown engine", "Failed delete dir recursive")
	_ = kv.SetEngineByName("etcd", settings)
	err = kv.SetEngineByName("keepass", settings)
	require.NoError(t, err, "Failed set engine by name")
}

func TestStorage_SetEngine(t *testing.T) {
	var engine Engine
	engine = &fakeEngine{}
	kv := Storage{}
	err := kv.SetEngine(engine, nil)
	require.NoError(t, err, "Failed set engine")
}

func TestStorage_InitEngine(t *testing.T) {
	err := kv.InitEngine(nil)
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.InitEngine(nil)
	require.NoError(t, err, "Failed init")
}

func TestStorage_SetDefaultDir(t *testing.T) {
	kv.engine = nil
	err := kv.SetDefaultDir("")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.SetDefaultDir("")
	require.NoError(t, err, "Failed set default dir")
}

func TestStorage_IsDir(t *testing.T) {
	kv.engine = nil
	_, err := kv.IsDir("")
	require.EqualError(t, err, "no engine")
	engineInit()
	_, err = kv.IsDir("")
	require.NoError(t, err, "Failed check dir")
}

func TestStorage_List(t *testing.T) {
	kv.engine = nil
	_, err := kv.List("")
	require.EqualError(t, err, "no engine")
	engineInit()
	_, err = kv.List("")
	require.NoError(t, err, "Failed list dir")
}

func TestStorage_ReadDir(t *testing.T) {
	kv.engine = nil
	_, err := kv.ReadDir("")
	require.EqualError(t, err, "no engine")
	engineInit()
	_, err = kv.ReadDir("")
	require.NoError(t, err, "Failed read dir")
}

func TestStorage_Get(t *testing.T) {
	kv.engine = nil
	_, err := kv.Get("")
	require.EqualError(t, err, "no engine")
	engineInit()
	_, err = kv.Get("")
	require.NoError(t, err, "Failed get data")
}

func TestData_Set(t *testing.T) {
	kv.engine = nil
	err := kv.Set("", "")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.Set("", "")
	require.NoError(t, err, "Failed set data")
}

func TestStorage_Delete(t *testing.T) {
	kv.engine = nil
	err := kv.Delete("")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.Delete("")
	require.NoError(t, err, "Failed delete data")
}

func TestStorage_CreateDir(t *testing.T) {
	kv.engine = nil
	err := kv.CreateDir("")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.CreateDir("")
	require.NoError(t, err, "Failed create dir")
}

func TestStorage_DeleteDir(t *testing.T) {
	kv.engine = nil
	err := kv.DeleteDir("")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.DeleteDir("")
	require.NoError(t, err, "Failed delete dir")
}

func TestStorage_DeleteDirRecursive(t *testing.T) {
	var err error
	kv.engine = nil
	err = kv.DeleteDirRecursive("")
	require.EqualError(t, err, "no engine")
	engineInit()
	err = kv.DeleteDirRecursive("")
	require.Error(t, err, "Failed delete dir recursive")
	err = kv.DeleteDirRecursive("/fail")
	require.Error(t, err, "Failed delete unknown dir recursive")
	err = kv.DeleteDirRecursive("/data/file")
	require.Error(t, err, "Failed delete dir recursive on file")
	err = kv.DeleteDirRecursive("/list")
	require.Error(t, err, "Failed delete dir recursive on file")
	err = kv.DeleteDirRecursive("/error_dir")
	require.Error(t, err, "Failed delete dir recursive")
	err = kv.DeleteDirRecursive("/error_file")
	require.Error(t, err, "Failed delete dir recursive")
	err = kv.DeleteDirRecursive("/data")
	require.NoError(t, err, "Failed delete dir recursive")
}

func TestStorage_ReadDirRecursive(t *testing.T) {
	var err error
	kv.engine = nil
	_, err = kv.ReadDirRecursive("")
	require.EqualError(t, err, "no engine")
	engineInit()
	var result CacheRaw
	result, err = kv.ReadDirRecursive("/fail")
	require.Error(t, err, "Failed read unknown dir recursive")
	result, err = kv.ReadDirRecursive("/data/file")
	require.Error(t, err, "Failed read dir recursive on file")
	result, err = kv.ReadDirRecursive("/list")
	require.Error(t, err, "Failed read dir recursive on error with List")
	result, err = kv.ReadDirRecursive("/error_dir1")
	require.Error(t, err, "Failed read dir recursive on dir with error")
	result, err = kv.ReadDirRecursive("/error_dir2")
	require.Error(t, err, "Failed read dir recursive on file with error")
	result, err = kv.ReadDirRecursive("/error_dir")
	require.NoError(t, err, "Failed read dir recursive on dir with error")
	require.Equal(t, CacheRaw{values: Values(nil), nodes: map[string]CacheRaw{"/error_dir/dir": {values: nil, nodes: map[string]CacheRaw{}}}}, result)
	result, err = kv.ReadDirRecursive("/recursive")
	require.NoError(t, err, "Failed read dir recursive")
	require.Equal(t, CacheRaw{values: Values(nil), nodes: map[string]CacheRaw{"/recursive/_data": {values: Values{"datafile": "zopa"},
		nodes: map[string]CacheRaw(nil)}, "/recursive/dir": {values: Values(nil),
		nodes: map[string]CacheRaw{"/recursive/dir/q": {values: Values{"username": "user"}, nodes: map[string]CacheRaw(nil)},
			"/recursive/dir/z": {values: Values{"data": "uahaha"}, nodes: map[string]CacheRaw(nil)}}}}}, result)
}

func TestCacheRaw_CreateCache(t *testing.T) {
	engineInit()
	raw, _ := kv.ReadDirRecursive("/recursive")
	result := raw.CreateCache(nil, true)
	require.Equal(t, Cache{"/recursive/dir/q": "{\"data\":\"zopa\",\"username\":\"user\"}", "/recursive/dir/z": "{\"data\":\"uahaha\"}"}, result)
	result = raw.CreateCache(map[string]string{}, false)
	require.Equal(t, Cache{"/recursive/_data": "{\"datafile\":\"zopa\"}", "/recursive/dir/q": "{\"username\":\"user\"}",
		"/recursive/dir/z": "{\"data\":\"uahaha\"}"}, result)
}

func TestPackCache(t *testing.T) {
	timestamp := time.Now().UTC()
	data := Cache{"root": "{\"level1\": \"data_level1\"}"}
	result, err := PackCache(data, timestamp)
	require.NoError(t, err, "Failed create cache")
	require.Equal(t, "{\"cache_time\":"+strconv.FormatInt(timestamp.UTC().Unix(), 10)+",\"data\":{\"root\":{\"level1\":\"data_level1\"}}}", result)
}

func TestUnpackCache(t *testing.T) {
	timestamp := time.Now().UTC().Truncate(time.Second)
	js := "{\"cache_time\":" + strconv.FormatInt(timestamp.UTC().Unix(), 10) + ",\"data\":{\"root\":{\"level1\":\"data_level1\"}}}"
	result, cacheTime, err := UnpackCache("fail")
	require.Error(t, err, "Failed parse cache")
	result, cacheTime, err = UnpackCache(js)
	require.NoError(t, err, "Failed parse cache")
	require.Equal(t, Cache{"root": "{\"level1\":\"data_level1\"}"}, result, "Failed parse cache - wrong data")
	require.Equal(t, timestamp, cacheTime, "Failed parse cache - incorrect time")
}

func TestStorage_CompareCache(t *testing.T) {
	engineInit()
	old := "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value1\"}, \"data2\":{\"key1\":\"value1\",\"key2\":\"value2\"}}}"
	oldCache, _, err := UnpackCache(old)
	require.NoError(t, err, "Failed parse cache")
	kv.SetCache(oldCache)

	// Удаление: удаляем параметр
	del := "{\"cache_time\":0,\"data\":{\"data2\":{\"key1\":\"value1\",\"key2\":\"value2\"}}}"
	delCache, _, _ := UnpackCache(del)
	result := kv.CompareCache(delCache)
	require.Equal(t, map[string][]string{"ADDED": {}, "CHANGED": {}, "DELETED": {"data1"}}, result)

	// Добавление: добавляем параметр
	add := "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value1\"}, \"data2\":{\"key1\":\"value1\",\"key2\":\"value2\"}, \"data3\":{\"key1\":\"value1\"}}}"
	addCache, _, _ := UnpackCache(add)
	result = kv.CompareCache(addCache)
	require.Equal(t, map[string][]string{"ADDED": {"data3"}, "CHANGED": {}, "DELETED": {}}, result)

	// Изменение: добавление нового ключа в параметр
	ch := "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value1\"}, \"data2\":{\"key1\":\"value1\",\"key2\":\"value2\", \"key3\":\"value3\"}}}"
	chCache, _, _ := UnpackCache(ch)
	result = kv.CompareCache(chCache)
	require.Equal(t, map[string][]string{"ADDED": {}, "CHANGED": {"data2"}, "DELETED": {}}, result)

	// Изменение: изменение существующего значения в ключе параметра
	ch = "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value2\"}, \"data2\":{\"key1\":\"value1\",\"key2\":\"value2\"}}}"
	chCache, _, _ = UnpackCache(ch)
	result = kv.CompareCache(chCache)
	require.Equal(t, map[string][]string{"ADDED": {}, "CHANGED": {"data1"}, "DELETED": {}}, result)

	// Изменение: удаление ключа из параметра
	ch = "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value1\"}, \"data2\":{\"key1\":\"value1\"}}}"
	chCache, _, _ = UnpackCache(ch)
	result = kv.CompareCache(chCache)
	require.Equal(t, map[string][]string{"ADDED": {}, "CHANGED": {"data2"}, "DELETED": {}}, result)

	// Ничего: изменение порядка параметров
	ch = "{\"cache_time\":0,\"data\":{\"data2\":{\"key2\":\"value2\", \"key1\":\"value1\"}, \"data1\":{\"key1\":\"value1\"}}}"
	chCache, _, _ = UnpackCache(ch)
	result = kv.CompareCache(chCache)
	require.Equal(t, map[string][]string{"ADDED": {}, "CHANGED": {}, "DELETED": {}}, result)
}

func TestStorage_GetCache(t *testing.T) {
	engineInit()
	raw := "{\"cache_time\":0,\"data\":{\"data1\":{\"key1\":\"value1\"}, \"data2\":{\"key1\":\"value1\",\"key2\":\"value2\"}}}"
	cache, _, err := UnpackCache(raw)
	require.NoError(t, err, "Failed parse cache")
	kv.SetCache(cache)
	result := kv.GetCache()
	require.Equal(t, Cache{"data1": "{\"key1\":\"value1\"}", "data2": "{\"key1\":\"value1\",\"key2\":\"value2\"}"}, result)
}

func TestStorage_Extract(t *testing.T) {
	kv.engine = nil
	engineInit()
	_, err := kv.Extract("data1")
	require.EqualError(t, err, "no cache")
	raw := "{\"cache_time\":0,\"data\":{\"data1\":{\"KEY1\":\"value1\", \"key2\":\"value2\"}}}"
	cache, _, err := UnpackCache(raw)
	require.NoError(t, err, "Failed parse cache")
	kv.SetCache(cache)
	result, err := kv.Extract("none")
	require.EqualError(t, err, "key not found")
	result, err = kv.Extract("data1")
	require.Equal(t, Values{"KEY1": "value1", "key2": "value2"}, result)
}

func TestStorage_ExtractLowerKey(t *testing.T) {
	kv.engine = nil
	engineInit()
	_, err := kv.ExtractLowerKey("data1")
	require.EqualError(t, err, "no cache")
	raw := "{\"cache_time\":0,\"data\":{\"data1\":{\"KEY1\":\"value1\", \"key2\":\"value2\"}}}"
	cache, _, err := UnpackCache(raw)
	require.NoError(t, err, "Failed parse cache")
	kv.SetCache(cache)
	result, err := kv.ExtractLowerKey("data1")
	require.NotEqual(t, Values{"KEY1": "value1", "key2": "value2"}, result)
	require.Equal(t, Values{"key1": "value1", "key2": "value2"}, result)
}
