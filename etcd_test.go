package kv

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

var etcd Etcd

func init() {
	http.HandleFunc("/", handler) // each request calls handler
	go func() {
		_ = http.ListenAndServe("localhost:2500", nil)
	}()
}

func handler(w http.ResponseWriter, r *http.Request) {
	var result string
	method := r.Method
	url := r.URL.Path
	query := r.URL.RawQuery
	body, _ := ioutil.ReadAll(r.Body)
	switch url {
	case "/version":
		result = "{\"etcdserver\":\"3.4.9\",\"etcdcluster\":\"3.4.0\"}"
	case "/v2/keys/create_dir":
		if query == "dir=true" {
			result = "{\"action\":\"set\",\"node\":{\"key\":\"/create_dir\",\"dir\":true,\"modifiedIndex\":24557574,\"createdIndex\":24557574}}"
		} else {
			result = "FAILED"
		}
	case "/v2/keys/create_dir_already_exists":
		w.WriteHeader(http.StatusForbidden)
		result = "{\"errorCode\":102,\"message\":\"Not a file\",\"cause\":\"/create_dir_already_exists\",\"index\":24561006}"

	case "/v2/keys/is_dir_no_dir":
		w.WriteHeader(http.StatusNotFound)
		result = "{\"errorCode\":100,\"message\":\"Key not found\",\"cause\":\"/is_dir_no_dir\",\"index\":24747767}"
	case "/v2/keys/is_dir_false":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/is_dir_false\",\"value\":\"\",\"modifiedIndex\":24878349,\"createdIndex\":24878349}}"
	case "/v2/keys/is_dir_true":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/is_dir_true\",\"dir\":true,\"modifiedIndex\":24557574,\"createdIndex\":24557574}}"

	case "/v2/keys/list_no_dir":
		w.WriteHeader(http.StatusNotFound)
		result = "{\"errorCode\":100,\"message\":\"Key not found\",\"cause\":\"/list_no_dir\",\"index\":25056391}"
	case "/v2/keys/list_file":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/list_file\",\"value\":\"\",\"modifiedIndex\":25050341,\"createdIndex\":25050341}}"
	case "/v2/keys/list_dir":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/list_dir\",\"dir\":true,\"nodes\":[{\"key\":\"/list_dir/data\",\"value\":\"data\"," +
			"\"modifiedIndex\":25049672,\"createdIndex\":25049672},{\"key\":\"/list_dir/dir\",\"dir\":true,\"modifiedIndex\":25082943," +
			"\"createdIndex\":25082943}],\"modifiedIndex\":24557574,\"createdIndex\":24557574}}"

	case "/v2/keys/delete_dir_no_dir":
		w.WriteHeader(http.StatusNotFound)
		result = "{\"errorCode\":100,\"message\":\"Key not found\",\"cause\":\"/delete_dir_no_dir\",\"index\":25402071}"
	case "/v2/keys/delete_dir_file":
		result = "{\"action\":\"delete\",\"node\":{\"key\":\"/delete_dir_file\",\"modifiedIndex\":25409121,\"createdIndex\":25109208}," +
			"\"prevNode\":{\"key\":\"/delete_dir_file\",\"value\":\"data\",\"modifiedIndex\":25109208,\"createdIndex\":25109208}}"
	case "/v2/keys/delete_dir_not_empty":
		if method == "DELETE" {
			w.WriteHeader(http.StatusForbidden)
			result = "{\"errorCode\":108,\"message\":\"Directory not empty\",\"cause\":\"/delete_dir_not_empty\",\"index\":25426274}"
		} else {
			result = "{\"action\":\"get\",\"node\":{\"key\":\"/delete_dir_not_empty\",\"dir\":true,\"nodes\":[{\"key\":\"/delete_dir_not_empty/data\"," +
				"\"value\":\"data\",\"modifiedIndex\":25105465,\"createdIndex\":25105465},{\"key\":\"/delete_dir_not_empty/dir\",\"dir\":true," +
				"\"modifiedIndex\":25082943,\"createdIndex\":25082943}],\"modifiedIndex\":24557574,\"createdIndex\":24557574}}"
		}
	case "/v2/keys/delete_dir_ok":
		result = "{\"action\":\"delete\",\"node\":{\"key\":\"/delete_dir_ok\",\"dir\":true,\"modifiedIndex\":25442805,\"createdIndex\":25441782}," +
			"\"prevNode\":{\"key\":\"/delete_dir_ok\",\"dir\":true,\"modifiedIndex\":25441782,\"createdIndex\":25441782}}"

	case "/v2/keys/get_nothing":
		w.WriteHeader(http.StatusNotFound)
		result = "{\"errorCode\":100,\"message\":\"Key not found\",\"cause\":\"/get_nothing\",\"index\":25505640}"
	case "/v2/keys/get_file":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/get_file\",\"value\":\"data\",\"modifiedIndex\":25434228,\"createdIndex\":25434228}}"
	case "/v2/keys/get_dir":
		result = "{\"action\":\"get\",\"node\":{\"key\":\"/get_dir\",\"dir\":true,\"nodes\":[{\"key\":\"/get_dir/data\",\"value\":\"data\"," +
			"\"modifiedIndex\":25105465,\"createdIndex\":25105465},{\"key\":\"/get_dir/dir\",\"dir\":true,\"modifiedIndex\":25082943," +
			"\"createdIndex\":25082943}],\"modifiedIndex\":24557574,\"createdIndex\":24557574}}"

	case "/v2/keys/set_dir":
		w.WriteHeader(http.StatusForbidden)
		result = "{\"errorCode\":102,\"message\":\"Not a file\",\"cause\":\"/set_dir\",\"index\":25543009}"
	case "/v2/keys/set_nothing":
		w.WriteHeader(http.StatusCreated)
		result = "{\"action\":\"set\",\"node\":{\"key\":\"/set_nothing\",\"value\":\"data\",\"modifiedIndex\":25556750,\"createdIndex\":25556750}}"
	case "/v2/keys/set_file":
		result = "{\"action\":\"set\",\"node\":{\"key\":\"/set_file\",\"value\":\"zzz\",\"modifiedIndex\":25563881,\"createdIndex\":25563881}," +
			"\"prevNode\":{\"key\":\"/set_file\",\"value\":\"data\",\"modifiedIndex\":25556750,\"createdIndex\":25556750}}"

	case "/v2/keys/delete_nothing":
		w.WriteHeader(http.StatusNotFound)
		result = "{\"errorCode\":100,\"message\":\"Key not found\",\"cause\":\"/delete_nothing\",\"index\":25580393}"
	case "/v2/keys/delete_dir":
		w.WriteHeader(http.StatusForbidden)
		result = "{\"errorCode\":102,\"message\":\"Not a file\",\"cause\":\"/delete_dir\",\"index\":25585595}"
	case "/v2/keys/delete_empty_dir":
		w.WriteHeader(http.StatusForbidden)
		result = "{\"errorCode\":102,\"message\":\"Not a file\",\"cause\":\"/delete_empty_dir\",\"index\":25590502}"
	case "/v2/keys/delete_file":
		result = "{\"action\":\"delete\",\"node\":{\"key\":\"/delete_file\",\"modifiedIndex\":25595828,\"createdIndex\":25563881}," +
			"\"prevNode\":{\"key\":\"/delete_file\",\"value\":\"zzz\",\"modifiedIndex\":25563881,\"createdIndex\":25563881}}"

	default:
		result = "unknown query"
		fmt.Printf("Method = %q\nURL = %q\nQuery = %q\nBody = %q\nResponse = %q\n", method, url, query, body, result)
	}
	_, _ = fmt.Fprint(w, result)
}

func etcdInit() {
	var settings = map[string]interface{}{}
	etcd = Etcd{}
	settings["address_list"] = []string{"http://localhost:2500"}
	_ = etcd.Init(settings)
}

func TestEtcd_Init(t *testing.T) {
	etcd := Etcd{}
	var settings = map[string]interface{}{}
	var err error

	//	Test incorrect address type
	settings["address_list"] = "data"
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on incorrect address type")

	//	Test missing address value
	settings["address_list"] = []string{}
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on missing address value")

	//	Test incorrect address value
	settings["address_list"] = []string{"invalid"}
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on incorrect address value")
	delete(settings, "address_list")

	//	Test incorrect address value with interface type
	settings["address_list"] = []interface{}{"invalid", "address"}
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on incorrect address value")
	delete(settings, "address_list")

	//	Test incorrect default dir type
	settings["default_dir"] = 100
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on incorrect default dir type")
	delete(settings, "default_dir")

	//	Test incorrect transport type
	settings["transport"] = true
	err = etcd.Init(settings)
	require.Error(t, err, "Failed test on incorrect transport type")
	delete(settings, "transport")

	settings["address_list"] = []string{"http://localhost:2500"}
	settings["default_dir"] = "/test"
	settings["transport"] = http.Transport{
		TLSHandshakeTimeout: 10 * time.Second,
	}
	err = etcd.Init(settings)
	require.NoError(t, err, "Failed set settings")
}

func TestEtcd_CreateDir(t *testing.T) {
	var err error

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	err = etcd.CreateDir("")
	require.Error(t, err, "Failed test on empty path")
	err = etcd.CreateDir("/create_dir_already_exists")
	require.Error(t, err, "Failed test on already exist dir")
	err = etcd.CreateDir("create_dir")
	require.NoError(t, err, "Failed set create dir")
}

func TestEtcd_IsDir(t *testing.T) {
	var err error
	var ok bool

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	_, err = etcd.IsDir("")
	require.Error(t, err, "Failed test on empty path")
	ok, err = etcd.IsDir("/is_dir_no_dir")
	require.Error(t, err, "Failed test on not exist dir")
	ok, err = etcd.IsDir("/is_dir_false")
	require.NoError(t, err, "Failed transport test on file")
	require.False(t, ok, "Failed test on file")
	ok, err = etcd.IsDir("is_dir_true")
	require.NoError(t, err, "Failed transport test on dir")
	require.True(t, ok, "Failed test on file")
}

func TestEtcd_List(t *testing.T) {
	var err error
	var data map[string]bool

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	_, err = etcd.List("")
	require.Error(t, err, "Failed test on empty path")
	data, err = etcd.List("/list_no_dir")
	require.Error(t, err, "Failed test on not exist dir")
	_, err = etcd.List("/list_file")
	require.Error(t, err, "Failed transport test on file")
	data, err = etcd.List("list_dir")
	require.NoError(t, err, "Failed test on dir")
	require.Equal(t, data, map[string]bool{"/list_dir/data": false, "/list_dir/dir": true}, "Failed test on dir")
}

func TestEtcd_ReadDir(t *testing.T) {
	var err error
	var data map[string]string

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	_, err = etcd.ReadDir("")
	require.Error(t, err, "Failed test on empty path")
	data, err = etcd.ReadDir("/list_no_dir")
	require.Error(t, err, "Failed test on not exist dir")
	_, err = etcd.ReadDir("/list_file")
	require.Error(t, err, "Failed transport test on file")
	data, err = etcd.ReadDir("list_dir")
	require.NoError(t, err, "Failed test on dir")
	require.Equal(t, data, map[string]string{"/list_dir/data": "data", "/list_dir/dir": ""}, "Failed test on dir")
}

func TestEtcd_DeleteDir(t *testing.T) {
	var err error

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	err = etcd.DeleteDir("")
	require.Error(t, err, "Failed test on empty path")
	err = etcd.DeleteDir("/delete_dir_no_dir")
	require.Error(t, err, "Failed test on not exist dir")
	err = etcd.DeleteDir("/delete_dir_file")
	require.Error(t, err, "Failed test on file")
	err = etcd.DeleteDir("/delete_dir_not_empty")
	require.Error(t, err, "Failed test on not empty dir")
	err = etcd.DeleteDir("delete_dir_ok")
	require.NoError(t, err, "Failed test on empty dir")
}

func TestEtcd_Get(t *testing.T) {
	var err error
	var data string

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	_, err = etcd.Get("")
	require.Error(t, err, "Failed test on empty path")
	_, err = etcd.Get("/get_nothing")
	require.Error(t, err, "Failed test on not exist")
	data, err = etcd.Get("/get_file")
	require.NoError(t, err, "Failed transport test on file")
	require.Equal(t, data, "data", "Failed test on file")
	data, err = etcd.Get("get_dir")
	require.NoError(t, err, "Failed transport test on dir")
	require.Equal(t, data, "", "Failed test on dir")
}

func TestEtcd_Set(t *testing.T) {
	var err error

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	err = etcd.Set("", "")
	require.Error(t, err, "Failed test on empty path")
	err = etcd.Set("/set_dir", "data")
	require.Error(t, err, "Failed test on dir")
	err = etcd.Set("/set_nothing", "data")
	require.NoError(t, err, "Failed test on not exist")
	err = etcd.Set("set_file", "zzz")
	require.NoError(t, err, "Failed test on file")
}

func TestEtcd_Delete(t *testing.T) {
	var err error

	//	Initialize
	if etcd.client == nil {
		etcdInit()
	}

	//	Tests
	err = etcd.Delete("")
	require.Error(t, err, "Failed test on empty path")
	err = etcd.Delete("/delete_nothing")
	require.Error(t, err, "Failed test on not exist")
	err = etcd.Delete("/delete_dir")
	require.Error(t, err, "Failed test on dir")
	err = etcd.Delete("/delete_empty_dir")
	require.Error(t, err, "Failed test on empty dir")
	err = etcd.Delete("delete_file")
	require.NoError(t, err, "Failed test on file")
}
