package kv

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/tobischo/gokeepasslib/v3"
	"os"
	"strings"
)

type KeePass struct {
	settings KeePassSettings
	database *gokeepasslib.Database
	password string
	data     *gokeepasslib.Group
}

type KeePassSettings struct {
	databaseFile string
	password     string
	defaultDir   string
	autosave	 *bool
}

func (keepass *KeePass) Init(incoming map[string]interface{}) error {
	settings := keepass.settings

	//	Fill incoming settings
	for key, value := range incoming {
		switch key {
		case "default_dir":
			if dir, ok := value.(string); ok {
				_ = keepass.SetDefaultDir(dir)
				settings.defaultDir = keepass.settings.defaultDir
			} else {
				return errors.New("wrong value for parameter default_dir")
			}
		case "database":
			if data, ok := value.(string); ok {
				settings.databaseFile = data
			} else {
				return errors.New("wrong value for parameter database")
			}
		case "password":
			if data, ok := value.(string); ok {
				settings.password = data
			} else {
				return errors.New("wrong value for parameter password")
			}
		case "autosave":
			if data, ok := value.(bool); ok {
				settings.autosave = &data
			} else {
				return errors.New("wrong value for parameter autosave")
			}
		}
	}
	keepass.settings = settings
	return keepass.Open(settings.databaseFile, settings.password)
}

func (keepass *KeePass) Open(databaseFile, password string) error {
	var err error
	var openFile *os.File

	if openFile, err = os.OpenFile(databaseFile, os.O_RDWR, 0644); err != nil {
		return fmt.Errorf("cannot open file %s, reason: %s", databaseFile, err)
	}
	defer func() {
		_ = openFile.Close()
	}()

	if keepass.settings.databaseFile == "" {
		keepass.settings.databaseFile = databaseFile
	}

	keepass.database = gokeepasslib.NewDatabase()
	keepass.database.Credentials = gokeepasslib.NewPasswordCredentials(password)

	if err = gokeepasslib.NewDecoder(openFile).Decode(keepass.database); err != nil {
		return fmt.Errorf("cannot create decoder, reason: %s", err)
	}

	// Unlock protected entries to handle stream cipher
	if err = keepass.database.UnlockProtectedEntries(); err != nil {
		return fmt.Errorf("unable to unprotect database, reason: %s", err)
	}

	keepass.data = &gokeepasslib.Group{}
	keepass.data.Groups = keepass.database.Content.Root.Groups
	return nil
}

func (keepass *KeePass) Save(filename string) error {
	var openFile *os.File
	//var offset int64
	var err error

	if openFile, err = os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0644); err != nil {
		return fmt.Errorf("cannot open file %s, reason: %s", filename, err)
	}
	defer func() {
		_ = openFile.Close()
	}()

	if _, err = openFile.Seek(0, 0); err != nil {
		return err
	}

	keepassEncoder := gokeepasslib.NewEncoder(openFile)
	if err := keepassEncoder.Encode(keepass.database); err != nil {
		return fmt.Errorf("cannot write file %s, reason: %s", filename, err)
	}
	return nil
}

func (keepass *KeePass) SetAutoSaveOn() {
	varBool := true
	keepass.settings.autosave = &varBool
}

func (keepass *KeePass) SetAutoSaveOff() {
	varBool := false
	keepass.settings.autosave = &varBool
}

func (keepass *KeePass) autoSave() error {
	if keepass.settings.autosave == nil {
		keepass.SetAutoSaveOn()
	}

	if *keepass.settings.autosave {
		return keepass.Save(keepass.settings.databaseFile)
	}
	return nil
}

func (keepass *KeePass) GetGroupByName(fullPath string) (*gokeepasslib.Group, *gokeepasslib.Entry, error) {
	var entry *gokeepasslib.Entry
	path := strings.Split(strings.Trim(fullPath, "/"), "/")
	pathIndex := 0
	rootGroup := keepass.data
	var check = true
	if len(path) > 1 || path[0] != "" {
		for _, value := range path {
			check = false
			for key, group := range rootGroup.Groups {
				if group.Name == value {
					rootGroup = &rootGroup.Groups[key]
					pathIndex++
					check = true
					break
				}
			}
			if !check {
				break
			}
		}
	}

	if !check {
		if pathIndex == len(path)-1 {
			for key, value := range rootGroup.Entries {
				if path[pathIndex] == value.GetTitle() {
					entry = &rootGroup.Entries[key]
					check = true
					break
				}
			}
		}
	}

	if !check {
		return nil, nil, fmt.Errorf("path not found")
	}
	return rootGroup, entry, nil
}

func (keepass *KeePass) GetEntryInfo(entry *gokeepasslib.Entry) (string, error) {
	if entry == nil || entry.Times.CreationTime == nil {
		return "", errors.New("empty entry")
	}
	result := make(map[string]string)
	for _, value := range entry.Values {
		if strings.ToLower(value.Key) != "title" {
			result[strings.ToLower(value.Key)] = entry.GetContent(value.Key)
		}
	}
	data, _ := json.Marshal(result)
	return string(data), nil
}

func (keepass *KeePass) SetDefaultDir(dir string) error {
	keepass.settings.defaultDir = "/" + strings.Trim(dir, "/") + "/"
	return nil
}

func (keepass *KeePass) CreateDir(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = "/" + strings.Trim(path, "/")

	parentPathSlice := strings.Split(path, "/")
	parentPath := strings.Join(parentPathSlice[0:len(parentPathSlice)-1], "/")
	parentGroup, _, err := keepass.GetGroupByName(parentPath)
	lastPath := parentPathSlice[len(parentPathSlice)-1]

	if err != nil {
		return fmt.Errorf("cannot get parent dir info on %s, reason: %s", path, err)
	}

	//	Check target path if group
	existsGroup := false
	for _, value := range parentGroup.Groups {
		if value.Name == lastPath {
			existsGroup = true
		}
	}

	//	Check target path if entry
	existsEntry := false
	for _, value := range parentGroup.Entries {
		if value.GetTitle() == lastPath {
			existsEntry = true
		}
	}

	//	Dir exists
	if existsGroup {
		return fmt.Errorf("dir already exists")
	}

	//	File with same name already exists
	if existsEntry {
		return fmt.Errorf("create dir over file")
	}

	group := gokeepasslib.NewGroup()
	group.Name = parentPathSlice[len(parentPathSlice)-1]

	parentGroup.Groups = append(parentGroup.Groups, group)

	return keepass.autoSave()
}

func (keepass *KeePass) IsDir(path string) (bool, error) {
	if len(path) == 0 {
		return false, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = "/" + strings.Trim(path, "/")

	_, entry, err := keepass.GetGroupByName(path)
	if err != nil {
		return false, err
	}
	return entry == nil, nil
}

func (keepass *KeePass) List(path string) (map[string]bool, error) {
	if len(path) == 0 {
		return nil, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = "/" + strings.Trim(path, "/")

	group, entry, err := keepass.GetGroupByName(path)
	if err != nil {
		return nil, fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}
	if entry != nil {
		return nil, fmt.Errorf("path %s is not dir", path)
	}
	result := make(map[string]bool)
	for _, n := range group.Groups {
		result[path+"/"+n.Name] = true
	}
	for _, n := range group.Entries {
		result[path+"/"+n.GetTitle()] = false
	}
	return result, nil
}

func (keepass *KeePass) ReadDir(path string) (map[string]string, error) {
	if len(path) == 0 {
		return nil, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = "/" + strings.Trim(path, "/")

	group, entry, err := keepass.GetGroupByName(path)
	if err != nil {
		return nil, fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}
	if entry != nil {
		return nil, fmt.Errorf("path %s is not dir", path)
	}
	result := make(map[string]string)
	for _, n := range group.Groups {
		result[path+"/"+n.Name] = ""
	}
	for key, n := range group.Entries {
		jsonValue, _ := keepass.GetEntryInfo(&group.Entries[key])
		result[path+"/"+n.GetTitle()] = jsonValue
	}
	return result, nil
}

func (keepass *KeePass) DeleteDir(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = strings.Trim(path, "/")

	parentPathSlice := strings.Split(path, "/")
	parentPath := strings.Join(parentPathSlice[0:len(parentPathSlice)-1], "/")
	parentGroup, _, err := keepass.GetGroupByName(parentPath)
	lastPath := parentPathSlice[len(parentPathSlice)-1]

	if err != nil {
		return fmt.Errorf("cannot get parent dir info on %s, reason: %s", path, err)
	}

	//	Check target path if group
	var group gokeepasslib.Group
	groupIndex := 0
	passedGroup := false
	for key, value := range parentGroup.Groups {
		if value.Name == lastPath {
			group = value
			groupIndex = key
			passedGroup = true
		}
	}

	//	Check target path if entry
	passedEntry := false
	for _, value := range parentGroup.Entries {
		if value.GetTitle() == lastPath {
			passedEntry = true
		}
	}

	//	Target path not exists
	if !passedEntry && !passedGroup {
		return fmt.Errorf("%s is not exists", path)
	}

	//	Target path is entry
	if passedEntry && !passedGroup {
		return fmt.Errorf("%s is not a dir", path)
	}

	//	Check target dir is not empty
	if len(group.Entries) > 0 || len(group.Groups) > 0 {
		return fmt.Errorf("dir is not empty")
	}
	parentGroup.Groups = append(parentGroup.Groups[:groupIndex], parentGroup.Groups[groupIndex+1:]...)

	return keepass.autoSave()
}

func (keepass *KeePass) Get(path string) (string, error) {
	if len(path) == 0 {
		return "", errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = "/" + strings.Trim(path, "/")

	_, entry, err := keepass.GetGroupByName(path)
	if err != nil {
		return "", fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}

	if info, err := keepass.GetEntryInfo(entry); err != nil {
		return "", fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	} else {
		return info, nil
	}
}

func (keepass *KeePass) Set(path, newValue string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = strings.Trim(path, "/")

	parentPathSlice := strings.Split(path, "/")
	parentPath := strings.Join(parentPathSlice[0:len(parentPathSlice)-1], "/")
	parentGroup, entry, err := keepass.GetGroupByName(parentPath)
	lastPath := parentPathSlice[len(parentPathSlice)-1]

	if err != nil {
		return fmt.Errorf("cannot get parent dir info on %s, reason: %s", path, err)
	}

	isGroup := false
	for _, value := range parentGroup.Groups {
		if value.Name == lastPath {
			isGroup = true
			break
		}
	}
	if isGroup {
		return fmt.Errorf("path is not file")
	}

	//	Check target path if exists
	entryExists := false
	for key, value := range parentGroup.Entries {
		if value.GetTitle() == lastPath {
			entry = &parentGroup.Entries[key]
			entryExists = true
		}
	}

	if !entryExists {
		newEntry := gokeepasslib.NewEntry()
		newEntry.Values = append(newEntry.Values, gokeepasslib.ValueData{Key: "Title", Value: gokeepasslib.V{Content: lastPath}})
		parentGroup.Entries = append(parentGroup.Entries, newEntry)
		entry = &parentGroup.Entries[len(parentGroup.Entries)-1]
	}

	//	Convert data to map (non JSON convert to username field)
	var data map[string]string
	err = json.Unmarshal([]byte(newValue), &data)
	if err != nil {
		data = map[string]string{"username": newValue}
	}
	if _, ok := data["Title"]; !ok {
		data["Title"] = lastPath
	}

	//	Replace data or create new, if needed
	for dataKey, dataValue := range data {
		passed := false
		for key, value := range entry.Values {
			if strings.ToLower(dataKey) == strings.ToLower(value.Key) {
				passed = true
				entry.Values[key].Value.Content = dataValue
			}
		}
		if !passed {
			entry.Values = append(entry.Values, gokeepasslib.ValueData{Key: dataKey, Value: gokeepasslib.V{Content: dataValue}})
		}
	}

	return keepass.autoSave()
}

func (keepass *KeePass) Delete(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = keepass.settings.defaultDir + path
	}
	path = strings.Trim(path, "/")

	group, entry, err := keepass.GetGroupByName(path)
	if err != nil {
		return fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}

	if entry == nil {
		return fmt.Errorf("path is not file")
	}

	for key, value := range group.Entries {
		if value.GetTitle() == entry.GetTitle() {
			group.Entries = append(group.Entries[:key], group.Entries[key+1:]...)
			break
		}
	}

	return keepass.autoSave()
}
