module gitlab.com/megafon-libraries/kv

go 1.14

require (
	bou.ke/monkey v1.0.2
	github.com/coreos/etcd v3.3.25+incompatible
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/tobischo/gokeepasslib/v3 v3.1.0
)
