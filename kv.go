package kv

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"
)

type Engine interface {
	Init(map[string]interface{}) error
	SetDefaultDir(string) error
	CreateDir(string) error
	IsDir(string) (bool, error)
	List(string) (map[string]bool, error)
	ReadDir(string) (map[string]string, error)
	DeleteDir(string) error
	Get(string) (string, error)
	Set(string, string) error
	Delete(string) error
}

type Cache map[string]string    // Формат кэша
type Internal map[string]Values // Формат внутреннего хранилища
type Values map[string]string   // Формат значений хранилища
type CacheRaw struct {          // Формат "сырого" кэша, получаемого из kv движков
	values Values
	nodes  map[string]CacheRaw
}

type CachePack struct {
	CacheTime int64                      `json:"cache_time"`
	Data      map[string]json.RawMessage `json:"data"`
}

type Storage struct {
	sync.RWMutex
	cache    Cache
	engine   Engine
	internal Internal
}

// IsInit возвращает признак работоспособности хранилища
func (dt *Storage) IsInit() bool {
	return dt.engine != nil
}

// SetEngineByName по имени устанавливает движок, с которым работает хранилище
func (dt *Storage) SetEngineByName(name string, settings map[string]interface{}) error {
	var engine Engine
	switch name {
	case "etcd":
		engine = &Etcd{}
	case "keepass":
		engine = &KeePass{}
	default:
		return errors.New("unknown engine")
	}
	return dt.SetEngine(engine, settings)
}

// SetEngine устанавливает движок, с которым работает хранилище
func (dt *Storage) SetEngine(engine Engine, settings map[string]interface{}) error {
	dt.engine = engine
	err := dt.engine.Init(settings)
	if err != nil {
		dt.engine = nil
	}
	return err
}

// InitEngine инициализация движка хранилища
func (dt *Storage) InitEngine(incoming map[string]interface{}) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.Init(incoming)
}

// SetDefaultDir устанавливает путь по умолчанию
func (dt *Storage) SetDefaultDir(dir string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.SetDefaultDir(dir)
}

// IsDir возвращает признак каталога по указанному пути
func (dt *Storage) IsDir(path string) (bool, error) {
	if !dt.IsInit() {
		return false, errors.New("no engine")
	}
	return dt.engine.IsDir(path)
}

// List выводит список значений по указанному пути с признаком дальнейшей вложенности
func (dt *Storage) List(path string) (map[string]bool, error) {
	if !dt.IsInit() {
		return nil, errors.New("no engine")
	}
	return dt.engine.List(path)
}

// ReadDir считывает всю информацию по указанному пути в формате ключ/значение. Для каталогов значение отсутствует (но это не является признаком каталога)
func (dt *Storage) ReadDir(path string) (map[string]string, error) {
	if !dt.IsInit() {
		return nil, errors.New("no engine")
	}
	return dt.engine.ReadDir(path)
}

// Get получает значение по указанному пути
func (dt *Storage) Get(path string) (string, error) {
	if !dt.IsInit() {
		return "", errors.New("no engine")
	}
	return dt.engine.Get(path)
}

// Set устанавливает значение по указанному пути
func (dt *Storage) Set(path, value string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.Set(path, value)
}

// Delete удаляет значение по указанному пути
func (dt *Storage) Delete(path string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.Delete(path)
}

// CreateDir создаёт подкаталог по указанному пути
func (dt *Storage) CreateDir(path string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.CreateDir(path)
}

// DeleteDir удаляет каталог по указанному пути
func (dt *Storage) DeleteDir(path string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	return dt.engine.DeleteDir(path)
}

// DeleteDirRecursive удаляет каталог по указанному пути, а также все вложенные в него данные
func (dt *Storage) DeleteDirRecursive(path string) error {
	if !dt.IsInit() {
		return errors.New("no engine")
	}
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if dir, err := dt.engine.IsDir(path); err != nil {
		return fmt.Errorf("cannot detect dir on path %s, reason: %s", path, err)
	} else {
		if !dir {
			return fmt.Errorf("%s is not a dir", path)
		}
	}

	list, err := dt.engine.List(path)
	if err != nil {
		return fmt.Errorf("cannot get content dir on path %s, reason: %s", path, err)
	}
	for key, isDir := range list {
		if isDir {
			if err = dt.DeleteDirRecursive(key); err != nil {
				return err
			}
		} else {
			if err = dt.engine.Delete(key); err != nil {
				return err
			}
		}
	}

	if err := dt.engine.DeleteDir(path); err != nil {
		return fmt.Errorf("cannot delete dir %s, reason: %s", path, err)
	}
	return nil
}

// ReadDirRecursive считывает всё по указанному пути, включая вложенные данные
func (dt *Storage) ReadDirRecursive(path string) (CacheRaw, error) {
	if !dt.IsInit() {
		return CacheRaw{}, errors.New("no engine")
	}
	result := CacheRaw{}
	isDir, err := dt.engine.IsDir(path)
	if err != nil {
		return CacheRaw{}, fmt.Errorf("cannot get info for path %s, reason: %s", path, err)
	}
	if isDir {
		list, err := dt.engine.List(path)
		if err != nil {
			return CacheRaw{}, fmt.Errorf("cannot get content dir on path %s, reason: %s", path, err)
		}

		if result.nodes == nil {
			result.nodes = map[string]CacheRaw{}
		}

		for key, isDir := range list {
			if isDir {
				if dir, err := dt.ReadDirRecursive(key); err != nil {
					return CacheRaw{}, err
				} else {
					result.nodes[key] = dir
				}
			} else {
				if str, err := dt.engine.Get(key); err != nil {
					return CacheRaw{}, err
				} else {
					data := CacheRaw{
						values: Values{},
					}
					err := json.Unmarshal([]byte(str), &data.values)
					if err != nil {
						data.values["data"] = str
					}
					result.nodes[key] = data
				}
			}
		}
	} else {
		return CacheRaw{}, fmt.Errorf("cannot read recursive on file, path %s", path)
	}

	return result, err
}

func (dt *Storage) internalCache(cache Cache) Internal {
	internal := Internal{}
	for key, value := range cache {
		tmp := map[string]string{}
		_ = json.Unmarshal([]byte(value), &tmp)
		internal[key] = tmp
	}
	return internal
}

// SetCache сохраняет кэш внутрь хранилища
func (dt *Storage) SetCache(cache Cache) {
	dt.Lock()
	defer dt.Unlock()

	dt.cache = cache
	dt.internal = dt.internalCache(cache)
}

// GetCache получает кэш из хранилища
func (dt *Storage) GetCache() Cache {
	dt.RLock()
	defer dt.RUnlock()

	return dt.cache
}

// CompareCache сверяет кэш с данными в хранилище
func (dt *Storage) CompareCache(cache Cache) map[string][]string {
	dt.RLock()
	defer dt.RUnlock()

	oldCache := dt.internalCache(dt.cache)
	newCache := dt.internalCache(cache)
	result := map[string][]string{"ADDED": make([]string, 0), "CHANGED": make([]string, 0), "DELETED": make([]string, 0)}

loop:
	for cacheKey, cacheValues := range oldCache {
		if _, ok := newCache[cacheKey]; !ok {
			result["DELETED"] = append(result["DELETED"], cacheKey)
			delete(oldCache, cacheKey)
			continue
		}
		for key := range cacheValues {
			if _, ok := newCache[cacheKey][key]; !ok || oldCache[cacheKey][key] != newCache[cacheKey][key] {
				result["CHANGED"] = append(result["CHANGED"], cacheKey)
				delete(oldCache, cacheKey)
				delete(newCache, cacheKey)
				continue loop
			}
			delete(oldCache[cacheKey], key)
			delete(newCache[cacheKey], key)
		}
		if len(newCache[cacheKey]) > 0 {
			result["CHANGED"] = append(result["CHANGED"], cacheKey)
		}
		delete(oldCache, cacheKey)
		delete(newCache, cacheKey)
	}
	for cacheKey := range newCache {
		result["ADDED"] = append(result["ADDED"], cacheKey)
	}
	return result
}

// Extract извлекает данные из внутреннего кэша по указанному пути
func (dt *Storage) Extract(path string) (Values, error) {
	dt.RLock()
	defer dt.RUnlock()

	if dt.internal == nil {
		return nil, errors.New("no cache")
	}

	value, ok := dt.internal[path]
	if !ok {
		value, ok = dt.internal["/"+path]
		if !ok {
			return nil, errors.New("key not found")
		}
	}
	return value, nil
}

// ExtractLowerKey извлекает данные из внутреннего кэша по указанному пути, преобразовывая ключи в нижний регистр
func (dt *Storage) ExtractLowerKey(path string) (Values, error) {
	value, err := dt.Extract(path)
	if err != nil {
		return value, err
	}
	result := Values{}
	for k, v := range value {
		result[strings.ToLower(k)] = v
	}
	return result, nil
}

// CreateCache создаёт кэш данных из сырого кэша
func (cache *CacheRaw) CreateCache(parentEnv map[string]string, allowInheritance bool) Cache {
	result := Cache{}
	if cache.nodes != nil {

		//	Обогащаем env
		if parentEnv == nil {
			parentEnv = map[string]string{}
		}
		env := map[string]string{}
		for key, value := range parentEnv {
			env[key] = value
		}

		if allowInheritance {
			for key, value := range cache.nodes {
				keySlice := strings.Split(key, "/")
				if len(keySlice) == 0 {
					continue
				}
				lastKey := keySlice[len(keySlice)-1]
				if lastKey[0] == '_' && value.values != nil {

					// Приоритет отдаём полю username и только если его нет, или оно пусто, то заполняем поле первым попавшимися данными
					if username, ok := value.values["username"]; ok && strings.TrimSpace(username) != "" {
						env[lastKey[1:]] = strings.TrimSpace(username)
						continue
					}
					for _, dataValue := range value.values {
						if strings.TrimSpace(dataValue) != "" {
							env[lastKey[1:]] = strings.TrimSpace(dataValue)
							break
						}
					}
				}
			}
		}
		for key, value := range cache.nodes {
			keySlice := strings.Split(key, "/")
			if len(keySlice) == 0 {
				continue
			}
			lastKey := keySlice[len(keySlice)-1]
			if lastKey[0] != '_' || value.values == nil || !allowInheritance {
				if value.values != nil {
					data := Cache{}
					for envKey, envValue := range env {
						data[envKey] = envValue
					}
					for dataKey, dataValue := range value.values {
						data[dataKey] = dataValue
					}
					jsonData, _ := json.Marshal(data)
					result[key] = string(jsonData)
				} else {
					cache := value.CreateCache(env, allowInheritance)
					for dataKey, dataValue := range cache {
						result[dataKey] = dataValue
					}
				}
			}
		}
	}
	return result
}

// PackCache упаковывает кэш в строку
func PackCache(source Cache, timestamp time.Time) (string, error) {
	data := CachePack{Data: map[string]json.RawMessage{}}

	for key, value := range source {
		data.Data[key] = []byte(value)
	}
	data.CacheTime = timestamp.Unix()
	result, err := json.Marshal(data)
	return string(result), err
}

// UnpackCache распаковывает кэш из строки
func UnpackCache(source string) (Cache, time.Time, error) {
	result := Cache{}
	data := CachePack{}
	if err := json.Unmarshal([]byte(source), &data); err != nil {
		return nil, time.Time{}, fmt.Errorf("invalid password data, reason: %s", err)
	}
	for key, value := range data.Data {
		result[key] = string(value)
	}
	timestamp := time.Unix(data.CacheTime, 0).UTC()
	return result, timestamp, nil
}
