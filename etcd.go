package kv

import (
	"context"
	"errors"
	"fmt"
	"github.com/coreos/etcd/client"
	"net/http"
	"strings"
	"time"
)

type Etcd struct {
	settings EtcdSettings
	client   client.Client
	api      client.KeysAPI
}

type EtcdSettings struct {
	address    []string
	transport  *http.Transport
	defaultDir string
}

func (etcd *Etcd) Init(incoming map[string]interface{}) error {
	var err error
	setting := etcd.settings

	//	Set default values if need
	if setting.defaultDir == "" {
		setting.defaultDir = "/"
	}
	if setting.transport == nil {
		setting.transport = &http.Transport{
			TLSHandshakeTimeout: 10 * time.Second,
		}
	}
	if setting.address == nil {
		setting.address = make([]string, 0)
	}

	//	Fill incoming settings
	for key, value := range incoming {
		switch key {
		case "default_dir":
			if dir, ok := value.(string); ok {
				_ = etcd.SetDefaultDir(dir)
				setting.defaultDir = etcd.settings.defaultDir
			} else {
				return errors.New("wrong value for parameter default_dir")
			}
		case "address_list":
			if slice, ok := value.([]string); ok {
				setting.address = make([]string, 0)
				for _, data := range slice {
					setting.address = append(setting.address, data)
				}
			} else if slice, ok := value.([]interface{}); ok {
				setting.address = make([]string, 0)
				for _, data := range slice {
					setting.address = append(setting.address, data.(string))
				}
			} else {
				return errors.New("wrong value for parameter address")
			}
		case "transport":
			if transport, ok := value.(http.Transport); ok {
				setting.transport = &transport
			} else {
				return errors.New("wrong value for parameter transport")
			}
		}
	}
	etcd.settings = setting
	cfg := client.Config{
		Endpoints: setting.address,
		Transport: setting.transport,
		// set timeout per request to fail fast when the target endpoint is unavailable
		HeaderTimeoutPerRequest: time.Second,
	}
	if etcd.client, err = client.New(cfg); err != nil {
		return fmt.Errorf("cannot create client, reason: %s", err)
	}
	etcd.api = client.NewKeysAPI(etcd.client)
	if _, err := etcd.client.GetVersion(context.Background()); err != nil {
		return fmt.Errorf("cannot get etcd version, reason: %s", err)
	}
	return nil
}

func (etcd *Etcd) SetDefaultDir(dir string) error {
	etcd.settings.defaultDir = "/" + strings.Trim(dir, "/") + "/"
	return nil
}

func (etcd *Etcd) CreateDir(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	_, err := etcd.api.Set(context.Background(), path, "", &client.SetOptions{Dir: true})
	if err != nil {
		return fmt.Errorf("cannot create dir %s, reason: %s", path, err)
	}
	return nil
}

func (etcd *Etcd) IsDir(path string) (bool, error) {
	if len(path) == 0 {
		return false, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	resp, err := etcd.api.Get(context.Background(), path, nil)
	if err != nil {
		return false, fmt.Errorf("cannot detect dir on path %s, reason: %s", path, err)
	}
	return resp.Node.Dir, nil
}

func (etcd *Etcd) List(path string) (map[string]bool, error) {
	if len(path) == 0 {
		return nil, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	resp, err := etcd.api.Get(context.Background(), path, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}
	if !resp.Node.Dir {
		return nil, fmt.Errorf("path %s is not dir", path)
	}
	result := make(map[string]bool)
	for _, n := range resp.Node.Nodes {
		result[n.Key] = n.Dir
	}
	return result, nil
}

func (etcd *Etcd) ReadDir(path string) (map[string]string, error) {
	if len(path) == 0 {
		return nil, errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	resp, err := etcd.api.Get(context.Background(), path, nil)
	if err != nil {
		return nil, fmt.Errorf("cannot get data from path %s, reason: %s", path, err)
	}
	if !resp.Node.Dir {
		return nil, fmt.Errorf("path %s is not dir, reason: %s", path, err)
	}
	result := make(map[string]string)
	for _, n := range resp.Node.Nodes {
		result[n.Key] = n.Value
	}
	return result, nil
}

func (etcd *Etcd) DeleteDir(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	if dir, err := etcd.IsDir(path); err != nil {
		return fmt.Errorf("cannot detect dir on path %s, reason: %s", path, err)
	} else {
		if !dir {
			return fmt.Errorf("%s is not a dir", path)
		}
	}

	if _, err := etcd.api.Delete(context.Background(), path, &client.DeleteOptions{Dir: true}); err != nil {
		return fmt.Errorf("cannot delete dir %s, reason: %s", path, err)
	}
	return nil
}

func (etcd *Etcd) Get(path string) (string, error) {
	if len(path) == 0 {
		return "", errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	resp, err := etcd.api.Get(context.Background(), path, nil)
	if err != nil {
		return "", fmt.Errorf("cannot get value from path %s, reason: %s", path, err)
	}
	return resp.Node.Value, nil
}

func (etcd *Etcd) Set(path, value string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	_, err := etcd.api.Set(context.Background(), path, value, nil)
	if err != nil {
		return fmt.Errorf("cannot set value %s on path %s, reason: %s", value, path, err)
	}
	return nil
}

func (etcd *Etcd) Delete(path string) error {
	if len(path) == 0 {
		return errors.New("invalid path length")
	}
	if path[0] != '/' {
		path = etcd.settings.defaultDir + path
	}

	_, err := etcd.api.Delete(context.Background(), path, nil)
	if err != nil {
		return fmt.Errorf("cannot delete value on path %s, reason: %s", path, err)
	}
	return nil
}
