package kv

import (
	"bou.ke/monkey"
	"fmt"
	"github.com/stretchr/testify/require"
	"github.com/tobischo/gokeepasslib/v3"
	"os"
	"reflect"
	"testing"
)

var keepass KeePass

func init() {
	var settings = map[string]interface{}{}
	settings["database"] = "test/database.kdbx"
	settings["password"] = "test"
	settings["autosave"] = false
	_ = keepass.Init(settings)
}

func TestKeePass_Init(t *testing.T) {
	var err error
	var settings = map[string]interface{}{}
	settings["database"] = "test/database.kdbx"
	settings["password"] = "test"
	settings["default_dir"] = "/DATA"
	settings["autosave"] = false
	k := KeePass{}
	err = k.Init(settings)
	require.NoError(t, err, "Failed initialization test")
	settings = map[string]interface{}{"default_dir": 0}
	err = k.Init(settings)
	require.Error(t, err, "Failed test on invalid default dir format")
	settings = map[string]interface{}{"database": 0}
	err = k.Init(settings)
	require.Error(t, err, "Failed test on invalid database format")
	settings = map[string]interface{}{"password": 0}
	err = k.Init(settings)
	require.Error(t, err, "Failed test on invalid password format")
	settings = map[string]interface{}{"autosave": 0}
	err = k.Init(settings)
	require.Error(t, err, "Failed test on invalid autosave format")
}

func TestKeePass_Open(t *testing.T) {
	var err error
	k := KeePass{}
	err = k.Open("test/database.kdbx", "test")
	require.NoError(t, err, "Failed open test")
	err = k.Open("fail.kdbx", "test")
	require.Error(t, err, "Failed test on invalid database path")
	err = k.Open("test/database.kdbx", "tost")
	require.Error(t, err, "Failed test on invalid password")

	//	Patch UnlockProtectedEntries function
	var guard *monkey.PatchGuard
	var Unlock *gokeepasslib.Database
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(Unlock), "UnlockProtectedEntries", func(_ *gokeepasslib.Database) error {
		guard.Unpatch()
		return fmt.Errorf("catched")
	})
	err = k.Open("test/database.kdbx", "test")
	require.Error(t, err, "Failed test on unlock protected entries")
}

func TestKeePass_Save(t *testing.T) {
	var guard *monkey.PatchGuard
	var filename = "test/test.kdbx"
	var err error

	err = keepass.Save(".")
	require.Error(t, err, "Failed save on invalid path")

	//	Patch Seek function
	var Seek *os.File
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(Seek), "Seek", func(file *os.File, offset int64, whence int) (ret int64, err error) {
		guard.Unpatch()
		return 0, fmt.Errorf("catched")
	})
	err = keepass.Save(filename)
	require.Error(t, err, "Failed save on seek")

	//	Patch Encode function
	var Encoder *gokeepasslib.Encoder
	guard = monkey.PatchInstanceMethod(reflect.TypeOf(Encoder), "Encode", func(encoder *gokeepasslib.Encoder, database *gokeepasslib.Database) error {
		guard.Unpatch()
		return fmt.Errorf("catched")
	})
	err = keepass.Save(filename)
	require.Error(t, err, "Failed save on encode")

	//	Save
	err = keepass.Save(filename)
	require.NoError(t, err, "Failed save")

	//	Delete created file
	defer func() {
		_ = os.Remove(filename)
	}()

	//	Autosave
	kp := KeePass{}
	_ = kp.Open(filename, "test")
	err = kp.CreateDir("/DATA/AUTOSAVE")
	require.NoError(t, err, "Failed autosave")
}

func TestKeePass_SetAutoSaveOn(t *testing.T) {
	backup := keepass.settings.autosave
	keepass.SetAutoSaveOn()
	keepass.settings.autosave = backup
}

func TestKeePass_SetAutoSaveOff(t *testing.T) {
	backup := keepass.settings.autosave
	keepass.SetAutoSaveOff()
	keepass.settings.autosave = backup
}

func TestKeePass_GetGroupByName(t *testing.T) {
	group, entry, err := keepass.GetGroupByName("/DATA/INVALID/PATH")
	require.Error(t, err, "Failed test on invalid database path")
	require.Nil(t, group, "Failed test on invalid database path")
	require.Nil(t, entry, "Failed test on invalid database path")
	group, entry, err = keepass.GetGroupByName("/DATA/EMPTY")
	require.NoError(t, err, "Failed test get path info on empty dir")
	require.NotNil(t, group, "Failed test get path info on empty dir")
	require.Nil(t, entry, "Failed test get path info on empty dir")
	group, entry, err = keepass.GetGroupByName("/DATA")
	require.NoError(t, err, "Failed test get path info on not empty dir")
	require.NotNil(t, group, "Failed test get path info on not empty dir")
	require.Nil(t, entry, "Failed test get path info on not empty dir")
	group, entry, err = keepass.GetGroupByName("/DATA/DIR1/FILE1")
	require.NoError(t, err, "Failed test get path info on file")
	require.NotNil(t, group, "Failed test get path info on file")
	require.NotNil(t, entry, "Failed test get path info on file")
}

func TestKeePass_GetEntryInfo(t *testing.T) {
	_, entry, _ := keepass.GetGroupByName("/DATA/DIR1/DIR2/FILE2")
	data, err := keepass.GetEntryInfo(nil)
	require.Error(t, err, "Failed test on invalid entry")
	require.Equal(t, "", data, "Failed test on invalid entry")
	data, err = keepass.GetEntryInfo(entry)
	require.NoError(t, err, "Failed test on valid entry")
	require.Equal(t, "{\"notes\":\"\",\"password\":\"password2\",\"url\":\"\",\"username\":\"user2\"}", data, "Failed test on valid entry")
}

func TestKeePass_SetDefaultDir(t *testing.T) {
	err := keepass.SetDefaultDir("/")
	require.NoError(t, err, "Failed test on set default dir")
}

func TestKeePass_CreateDir(t *testing.T) {
	err := keepass.CreateDir("")
	require.Error(t, err, "Failed test on create dir without path")
	err = keepass.CreateDir("/DATA/INVALID/DIR3")
	require.Error(t, err, "Failed test on create dir with invalid path (invalid parent dir)")
	err = keepass.CreateDir("/DATA/DIR1/DIR2")
	require.Error(t, err, "Failed test on create dir with invalid path (dir already exists)")
	err = keepass.CreateDir("/DATA/DIR1/FILE1")
	require.Error(t, err, "Failed test on create dir with invalid path (dir create over file)")
	err = keepass.CreateDir("DATA/DIR1/DIR3")
	require.NoError(t, err, "Failed test on create dir")
	data, _ := keepass.List("/DATA/DIR1")
	require.Equal(t, map[string]bool{"/DATA/DIR1/DIR2": true, "/DATA/DIR1/DIR3": true, "/DATA/DIR1/FILE1": false}, data, "Failed test on create dir")
}

func TestKeePass_IsDir(t *testing.T) {
	data, err := keepass.IsDir("")
	require.Error(t, err, "Failed test on dir check without path")
	_, err = keepass.IsDir("/DATA/INVALID")
	require.Error(t, err, "Failed test on dir check with invalid path")
	data, err = keepass.IsDir("/DATA/EMPTY")
	require.NoError(t, err, "Failed test on dir check with empty dir")
	require.Equal(t, true, data, "Failed test on dir check with empty dir")
	data, err = keepass.IsDir("/DATA/DIR1")
	require.NoError(t, err, "Failed test on dir check with non empty dir")
	require.Equal(t, true, data, "Failed test on dir check with non empty dir")
	data, err = keepass.IsDir("DATA/FILE")
	require.NoError(t, err, "Failed test on dir check with file")
	require.Equal(t, false, data, "Failed test on dir check with file")
}

func TestKeePass_List(t *testing.T) {
	data, err := keepass.List("")
	require.Error(t, err, "Failed test list without path")
	_, err = keepass.List("/DATA/INVALID")
	require.Error(t, err, "Failed test list on invalid path")
	_, err = keepass.List("/DATA/FILE")
	require.Error(t, err, "Failed test list on file")
	data, err = keepass.List("/DATA/EMPTY")
	require.NoError(t, err, "Failed test list on empty dir")
	require.Equal(t, map[string]bool{}, data, "Failed test list on empty dir")
	data, err = keepass.List("/DATA")
	require.NoError(t, err, "Failed test list on non empty dir")
	require.Equal(t, map[string]bool{"/DATA/DIR1": true, "/DATA/EMPTY": true, "/DATA/EMPTY2": true, "/DATA/FILE": false}, data, 
	"Failed test list on non empty dir")
	data, err = keepass.List("DATA/DIR1/DIR2")
	require.NoError(t, err, "Failed test list on non empty dir")
	require.Equal(t, map[string]bool{"/DATA/DIR1/DIR2/FILE2": false, "/DATA/DIR1/DIR2/FILE3": false}, data, "Failed test list on non empty dir")
}

func TestKeePass_ReadDir(t *testing.T) {
	data, err := keepass.ReadDir("")
	require.Error(t, err, "Failed test read dir without path")
	_, err = keepass.ReadDir("/DATA/INVALID")
	require.Error(t, err, "Failed test read dir on invalid path")
	_, err = keepass.ReadDir("/DATA/FILE")
	require.Error(t, err, "Failed test read dir on file")
	data, err = keepass.ReadDir("/DATA/EMPTY")
	require.NoError(t, err, "Failed test read dir on empty dir")
	require.Equal(t, map[string]string{}, data, "Failed test list on empty dir")
	data, err = keepass.ReadDir("/DATA")
	require.NoError(t, err, "Failed test read dir on non empty dir")
	require.Equal(t, map[string]string{"/DATA/DIR1": "", "/DATA/EMPTY": "", "/DATA/EMPTY2": "", "/DATA/FILE": "{\"field\":\"fieldValue\",\"notes\":\"Notes\"," +
		"\"password\":\"password\",\"protectedfield\":\"protectedValue\",\"url\":\"https://keepass.info/\",\"username\":\"user\"}"}, data,
		"Failed test list on non empty dir")
	data, err = keepass.ReadDir("DATA/DIR1/DIR2")
	require.NoError(t, err, "Failed test read dir on non empty dir")
	require.Equal(t, map[string]string{"/DATA/DIR1/DIR2/FILE2": "{\"notes\":\"\",\"password\":\"password2\",\"url\":\"\"," +
		"\"username\":\"user2\"}", "/DATA/DIR1/DIR2/FILE3": "{\"notes\":\"\",\"password\":\"password3\",\"url\":\"\"," +
		"\"username\":\"user3\"}"}, data, "Failed test list on non empty dir")
}

func TestKeePass_DeleteDir(t *testing.T) {
	err := keepass.DeleteDir("")
	require.Error(t, err, "Failed test delete dir without path")
	err = keepass.DeleteDir("/DATA/INVALID")
	require.Error(t, err, "Failed test delete dir on invalid path")
	err = keepass.DeleteDir("/DATA/INVALID/DATA")
	require.Error(t, err, "Failed test delete dir on invalid parent path")
	err = keepass.DeleteDir("/DATA/FILE")
	require.Error(t, err, "Failed test delete dir on file")
	err = keepass.DeleteDir("/DATA/DIR1")
	require.Error(t, err, "Failed test delete non empty dir")
	_ = keepass.CreateDir("/DATA/DIR1/DIR3")
	data, _ := keepass.List("/DATA/DIR1")
	require.Equal(t, map[string]bool{"/DATA/DIR1/DIR2": true, "/DATA/DIR1/DIR3": true, "/DATA/DIR1/FILE1": false}, data, "Failed test on delete dir")
	err = keepass.DeleteDir("DATA/DIR1/DIR3")
	require.NoError(t, err, "Failed test delete dir")
	data, _ = keepass.List("/DATA/DIR1")
	require.Equal(t, map[string]bool{"/DATA/DIR1/DIR2": true, "/DATA/DIR1/FILE1": false}, data, "Failed test on delete dir")
}

func TestKeePass_Get(t *testing.T) {
	_, err := keepass.Get("")
	require.Error(t, err, "Failed test get without path")
	_, err = keepass.Get("/DATA/INVALID")
	require.Error(t, err, "Failed test get on invalid path")
	_, err = keepass.Get("/DATA/INVALID/DATA")
	require.Error(t, err, "Failed test get on invalid parent path")
	_, err = keepass.Get("/DATA/EMPTY")
	require.Error(t, err, "Failed test get on dir")
	data, err := keepass.Get("DATA/DIR1/DIR2/FILE2")
	require.NoError(t, err, "Failed test get on file")
	require.Equal(t, "{\"notes\":\"\",\"password\":\"password2\",\"url\":\"\",\"username\":\"user2\"}", data, "Failed test on get file")
}

func TestKeePass_Set(t *testing.T) {
	var data string
	var err error
	err = keepass.Set("", "")
	require.Error(t, err, "Failed test set without path")
	err = keepass.Set("/DATA/INVALID/DATA", "")
	require.Error(t, err, "Failed test set on invalid parent path")
	err = keepass.Set("/DATA/EMPTY", "")
	require.Error(t, err, "Failed test set on dir")
	err = keepass.Set("/DATA/EMPTY/FILE", "/DATA")
	require.NoError(t, err, "Failed test set on new file")
	data, err = keepass.Get("/DATA/EMPTY/FILE")
	require.NoError(t, err, "Failed test set on new file")
	require.Equal(t, "{\"username\":\"/DATA\"}", data, "Failed test on set on new file")
	err = keepass.Set("DATA/EMPTY/FILE", "{\"username\": \"new data\"}")
	require.NoError(t, err, "Failed test set on exists file")
	data, err = keepass.Get("/DATA/EMPTY/FILE")
	require.NoError(t, err, "Failed test set on exists file")
	require.Equal(t, "{\"username\":\"new data\"}", data, "Failed test on set on exists file")
}

func TestKeePass_Delete(t *testing.T) {
	err := keepass.Delete("")
	require.Error(t, err, "Failed test delete without path")
	err = keepass.Delete("/DATA/INVALID")
	require.Error(t, err, "Failed test delete on invalid parent path")
	err = keepass.Delete("/DATA/EMPTY")
	require.Error(t, err, "Failed test delete dir")
	err = keepass.Set("/DATA/EMPTY/FILE", "/DATA")
	require.NoError(t, err, "Failed test set on new file")
	data, _ := keepass.Get("/DATA/EMPTY/FILE")
	require.Equal(t, "{\"username\":\"/DATA\"}", data, "Failed test on set on new file")
	err = keepass.Delete("DATA/EMPTY/FILE")
	require.NoError(t, err, "Failed test delete file")
	_, err = keepass.Get("/DATA/EMPTY/FILE")
	require.Error(t, err, "Failed test delete file")
}